         HEADERS += $$DATAMAN_H/f_ejdb.h \
                    $$DATAMAN_H/f_ejdb_file.h \
                    $$DATAMAN_H/v_json.h \
                    $$DATAMAN_H/help.h \
                    $$DATAMAN_H/v_user.h \
                    $$DATAMAN_H/fmodel.h \
                    $$KEYS_H/keywords.h \
                    $$DATAMAN_H/v_yaml.h


         SOURCES += $$DATAMAN_CPP/f_ejdb.cpp \
                    $$DATAMAN_CPP/f_ejdb_file.cpp \
                    $$DATAMAN_CPP/v_json.cpp \
                    $$DATAMAN_CPP/help.cpp \
                    $$DATAMAN_CPP/main.cpp \
                    $$DATAMAN_CPP/v_user.cpp \
                    $$DATAMAN_CPP/gemsfit_iobson.cpp \
                    $$DATAMAN_CPP/fmodel.cpp \
                    $$DATAMAN_CPP/v_yaml.cpp


