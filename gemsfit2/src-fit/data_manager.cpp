/// \file data_manager.cpp
/// Implementation of reading database parameters form input file
/// and experimental data form databsse
//
// Copyright (C) 2013-2014 G.D.Miron, D.Kulik
// <GEMS Development Team, mailto:gems2.support@psi.ch>
//
// This file is part of the GEMSFIT2 code for parameterization of thermodynamic
// data and models <http://gems.web.psi.ch/GEMSFIT2/>
//
// GEMSIFT is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.

// GEMSFIT2 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GEMSFIT2 code. If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------
//
//

/**
 *	@file data_manager.cpp
 *
 *	@brief this source file contains the implementation of the data manager class,
 *	which retrieves and stores data from thre GEMSFIT2 input file as well as
 *	the measurement form an EJDB local database.
 *
 *	@author G. Dan Miron
 *
 * 	@date 26.03.2013
 *
 */

#include "data_manager.h"
#include "gemsfit_iofiles.h"
#include "keywords.h"
#include <jansson.h>
#ifdef buildWIN32
#include <tcejdb/ejdb.h>
#else
#include "ejdb.h"
#endif
//#include <sstream>
#include "json_parse.h"
#include "fstream"

std::ostream& operator<<(std::ostream& stream, const Data_Manager::DataSynonyms& data);
std::ostream& operator<<(std::ostream& stream, const Data_Manager::PhSyn& data);
std::ostream& operator<<(std::ostream& stream, const Data_Manager::samples& data);

// Constructor
Data_Manager::Data_Manager( )
{
    mode = gpf->KeysNdx;
    h_datasetlist = false;
    // Read parameters for database connection
    gpf->flog << "02. data_manager.cpp(51). Reading database parameter get_db_specs(); " << std::endl;
    get_db_specs_txt();

    // Getting the query result data into the Data_Manager class
    gpf->flog << "03. data_manager.cpp(55). Getting data form the EJDB database; " << std::endl;
    get_EJDB();

    gpf->flog << "04. data_manager.cpp(58). Getting distinct T and P pairs; " << std::endl;
    get_distinct_TP();

    get_DataSyn();
    check_Syn();
    mode = gpf->KeysNdx;
}


// Destructor
Data_Manager::~Data_Manager( )
{
    // Delete measurement data std::vector of experiments
    for (unsigned int i=0; i<experiments.size(); ++i)
    {
        delete experiments[i];
        delete bICv[i];
    }  
}

void Data_Manager::get_DataSyn()
{
    std::vector<std::string> out, out2, out3;
    Data_Manager::DataSynonyms ss;
    Data_Manager::PhSyn sss;
    parse_JSON_object(DataSyn, keys::PhNames[mode], out);
    for (unsigned i = 0; i < out.size(); i++)
    {

        SynPHs.push_back(sss);
        parse_JSON_object(out[i], keys::NameSys[mode], out2);
        SynPHs[SynPHs.size()-1].GemsName = out2[0];
        out2.clear();
        parse_JSON_object(out[i], keys::Syn[mode], out2);
        if (out2[0] != "")
        SynPHs[SynPHs.size()-1].syn = out2;
        out2.clear();

        parse_JSON_object(out[i], keys::DcNames[mode], out3);
        for (unsigned i = 0; i < out3.size(); i++)
        {
            SynPHs[SynPHs.size()-1].SynDCs.push_back(ss);
            parse_JSON_object(out3[i], keys::NameSys[mode], out2);
            SynPHs[SynPHs.size()-1].SynDCs[SynPHs[SynPHs.size()-1].SynDCs.size()-1].GemsName = out2[0];
            out2.clear();
            parse_JSON_object(out3[i], keys::Syn[mode], out2);
            if (out2[0] != "")
            SynPHs[SynPHs.size()-1].SynDCs[SynPHs[SynPHs.size()-1].SynDCs.size()-1].syn = out2;
            out2.clear();
        }
        out3.clear();
    }
    out.clear();

    parse_JSON_object(DataSyn, keys::PhPropNames[mode], out);
    for (unsigned i = 0; i < out.size(); i++)
    {

        SynPHPs.push_back(ss);
        parse_JSON_object(out[i], keys::NameSys[mode], out2);
        SynPHPs[SynPHPs.size()-1].GemsName = out2[0];
        out2.clear();
        parse_JSON_object(out[i], keys::Syn[mode], out2);
        if (out2[0] != "")
        SynPHPs[SynPHPs.size()-1].syn = out2;
        out2.clear();

    }
    out.clear();

    parse_JSON_object(DataSyn, keys::PropertyNames[mode], out);
    for (unsigned i = 0; i < out.size(); i++)
    {

        SynProps.push_back(ss);
        parse_JSON_object(out[i], keys::NameSys[mode], out2);
        SynProps[SynProps.size()-1].GemsName = out2[0];
        out2.clear();
        parse_JSON_object(out[i], keys::Syn[mode], out2);
        if (out2[0] != "")
        SynProps[SynProps.size()-1].syn = out2;
        out2.clear();
    }
    out.clear();


    parse_JSON_object(DataSyn, keys::DcPropNames[mode], out);
    for (unsigned i = 0; i < out.size(); i++)
    {
        SynDCPs.push_back(ss);
        parse_JSON_object(out[i], keys::NameSys[mode], out2);
        SynDCPs[SynDCPs.size()-1].GemsName = out2[0];
        out2.clear();
        parse_JSON_object(out[i], keys::Syn[mode], out2);
        if (out2[0] != "")
        SynDCPs[SynDCPs.size()-1].syn = out2;
        out2.clear();
    }
    out.clear();

#ifdef CHECK_LOAD
    std::fstream test_out("datasyn.log", std::ios::out);
    test_out << "SynPHs\n";
    for (auto const& item : SynPHs) {
           test_out << item << "\n";
    }
    test_out << "SynPHPs\n";
    for (auto const& item : SynPHPs) {
           test_out << item << "\n";
    }
    test_out << "SynDCPs\n";
    for (auto const& item : SynDCPs) {
           test_out << item << "\n";
    }
    test_out << "SynProps\n";
    for (auto const& item : SynProps) {
           test_out << item << "\n";
    }
#endif

}

void Data_Manager::check_Syn()
{
    // phases
    for (unsigned p = 0; p < SynPHs.size(); p++)
    {
        for (unsigned i = 0; i < experiments.size(); i++)
        {
            // phase name
            for (unsigned ep = 0; ep < experiments[i]->expphases.size(); ep++)
            {
                bool check = false;
                for (unsigned s = 0; s < SynPHs[p].syn.size(); s++)
                {
                    if ( (experiments[i]->expphases[ep]->phase == SynPHs[p].syn[s]))
                        check = true;
                    if (check)
                    {
                        experiments[i]->expphases[ep]->phase = SynPHs[p].GemsName;
                        check = false;
                        break;
                    }
                }
            }

            // DCs
            for (unsigned d = 0; d < SynPHs[p].SynDCs.size(); d++)
            {
                // DC name
                for (unsigned ep = 0; ep < experiments[i]->expphases.size(); ep++)
                {
                    for (unsigned dc = 0; dc < experiments[i]->expphases[ep]->phDC.size(); dc++)
                    {
                        bool check = false;
                        for (unsigned s = 0; s < SynPHs[p].SynDCs[d].syn.size(); s++)
                        {
                            if ((experiments[i]->expphases[ep]->phDC[dc]->DC == SynPHs[p].SynDCs[d].syn[s]) && (experiments[i]->expphases[ep]->phase == SynPHs[p].GemsName))
                                check = true;
                            if (check)
                            {
                                experiments[i]->expphases[ep]->phDC[dc]->DC = SynPHs[p].SynDCs[d].GemsName;
                                check = false;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    // DC properties
    for (unsigned d = 0; d < SynDCPs.size(); d++)
    {
        for (unsigned i = 0; i < experiments.size(); i++)
        {
            // DC prop
            for (unsigned ep = 0; ep < experiments[i]->expphases.size(); ep++)
            {
                for (unsigned dc = 0; dc < experiments[i]->expphases[ep]->phDC.size(); dc++)
                {
                    for (unsigned dcp = 0; dcp < experiments[i]->expphases[ep]->phDC[dc]->DCprop.size(); dcp++ )
                    {
                        bool check = false;
                        for (unsigned s = 0; s < SynDCPs[d].syn.size(); s++)
                        {
                            if ((experiments[i]->expphases[ep]->phDC[dc]->DCprop[dcp]->property == SynDCPs[d].syn[s]))
                                check = true;
                            if (check)
                            {
                                experiments[i]->expphases[ep]->phDC[dc]->DCprop[dcp]->property = SynDCPs[d].GemsName;
                                check = false;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    // Phase properties
    for (unsigned p = 0; p < SynPHPs.size(); p++)
    {
        for (unsigned i = 0; i < experiments.size(); i++)
        {
            // phase prop
            for (unsigned ep = 0; ep < experiments[i]->expphases.size(); ep++)
            {
                for (unsigned php = 0; php < experiments[i]->expphases[ep]->phprop.size(); php++ )
                {
                    bool check = false;
                    for (unsigned s = 0; s < SynPHPs[p].syn.size(); s++)
                    {
                        if ( (experiments[i]->expphases[ep]->phprop[php]->property == SynPHPs[p].syn[s]))
                            check = true;
                        if (check)
                        {
                            experiments[i]->expphases[ep]->phprop[php]->property = SynPHPs[p].GemsName;
                            check = false;
                            break;
                        }
                    }
                }
            }
        }
    }

}

void Data_Manager::get_db_specs_txt()
{
    std::string fname;
    std::vector<std::string> out, out2;
    int mode = gpf->KeysNdx;

    fname = gpf->OptParamFile();

    std::ifstream file(fname.c_str());
    std::ostringstream tmp;
    tmp<<file.rdbuf();
    std::string s = tmp.str();
//    std::std::cout<<s<<std::std::endl;

    parse_JSON_object(s, keys::DBPath[0], out);
    if (out.size() == 0)
    {
        parse_JSON_object(s, keys::DBPath[1], out);
     mode = 1; gpf->KeysNdx = 1;
    }
    if (out.size() == 0) {std::cout << "Error: No keyword for \""
                               <<keys::DBPath[0]<<"\" or \""
                               <<keys::DBPath[1]<<"\" found in the task definition"
                               << std::endl; exit(1);}
    DBname = out[0];
    out.clear();

    parse_JSON_object(s, keys::DBColl[mode], out);
    if (out.size() == 0) {std::cout << "Error: No keyword for \""
                               <<keys::DBColl[mode]<<"\" found in the task definition"
                               << std::endl; exit(1);}
    collection = out[0];
    out.clear();

    parse_JSON_object(s, keys::DSelect[mode], out);
    if (out.size() == 0) {std::cout << "Error: No keyword for \""
                               <<keys::DSelect[mode]<<"\" found in the task definition"
                               << std::endl; exit(1);}
    DataSelect = out[0];
    out.clear();

    parse_JSON_object(s, keys::DTarget[mode], out);
    if (out.size() == 0) {std::cout << "Error: No keyword for \""
                               <<keys::DTarget[mode]<<"\" found in the task definition"
                               << std::endl; exit(1);}
    DataTarget = out[0];
    out.clear();

    parse_JSON_object(s, keys::LogK[mode], out);
    if (out[0] != "") LogK = out;
    out.clear();

    parse_JSON_object(s, keys::G3Ksys[mode], out);
    if (out.size() == 0) {std::cout << "Error: No keyword for \""
                               <<keys::G3Ksys[mode]<<"\" found in the task definition"
                               << std::endl; exit(1);}
#ifdef buildWIN32
    std::replace( out[0].begin(), out[0].end(), '/', '\\');
#endif
    gpf->setGEMS3LstFilePath(out[0].c_str());
    out.clear();

    parse_JSON_object(s, keys::DatLogK[mode], out);
    DataLogK = out[0];
    out.clear();

    parse_JSON_object(s, keys::DataSyn[mode], out);
    DataSyn = out[0];
    out.clear();

    parse_JSON_object(s, keys::OptSet[mode], out);
    if (out.size() == 0) {std::cout << "Error: No keyword for \""
                               <<keys::OptSet[mode]<<"\" found in the task definition"
                               << std::endl; exit(1);}
    parse_JSON_object(out[0], keys::MPI[mode], out2);
    MPI = atoi(out2[0].c_str());
    out2.clear();

}


// Reading data from EJDB database
void Data_Manager::get_EJDB( )
{
//    typedef std::vector<int>     int_v;
    typedef std::vector<double>  double_v;
    typedef std::vector<std::string>  string_v;
    json_t *root;
    json_error_t jerror;

    string_v out, out2, usesample, skipsample, usedataset, skipdataset, skippdatasets,skippsamples, usepdatasets, usepsamples, SA, DS ;
    double_v qsT, qsP, WT;
    unsigned int Nsamples = 0 , Ndatasets = 0;

    std::stringstream ss;
    std::string sss;

    const char * JSON = DataSelect.c_str();
    root = json_loads(JSON, 0, &jerror);

    if(root)
    {
        // processing DataSelect
        parse_JSON_object(DataSelect, keys::usesamples, out);
        usesample = out; // query for selecting samples
        out.clear();

        parse_JSON_object(DataSelect, keys::skipsamples, out);
        skipsample = out; // query for selecting samples
        out.clear();

        parse_JSON_object(DataSelect, keys::usedatasets, out);
        usedataset = out; // query for selecting expdatasets
        out.clear();

        parse_JSON_object(DataSelect, keys::skipdatasets, out);
        skipdataset = out; // query for skipping expdatasets
        out.clear();

        parse_JSON_array_object(DataSelect, keys::usepair, keys::dataset, out);
        if (out.size() == 0) parse_JSON_array_object(DataSelect, keys::usepair, keys::usedatasets, out);
        usepdatasets = out; // query for skipping expdatasets
        out.clear();

        parse_JSON_array_object(DataSelect, keys::usepair, keys::samples, out);
        usepsamples = out; // query for skipping expdatasets
        out.clear();

        parse_JSON_array_object(DataSelect, keys::skippair, keys::dataset, out);
        if (out.size() == 0) parse_JSON_array_object(DataSelect, keys::skippair, keys::skipdatasets, out);
        skippdatasets = out; // query for skipping expdatasets
        out.clear();

        parse_JSON_array_object(DataSelect, keys::skippair, keys::samples, out);
        skippsamples = out; // query for skipping expdatasets
        out.clear();

        parse_JSON_object(DataSelect, keys::sT, out);
        for (unsigned int i = 0 ; i < out.size() ; i++)
        {
            qsT.push_back( atof(out.at(i).c_str()) ); // query for selecting T
        }
        out.clear();

        parse_JSON_object(DataSelect, keys::sP, out);
        for (unsigned int i = 0 ; i < out.size() ; i++)
        {
            qsP.push_back( atof(out.at(i).c_str()) ); // query for selecting P
        }
        out.clear();


        parse_JSON_object(DataSelect, keys::samplelist, out);
        Nsamples = out.size();
        for (unsigned int i = 0; i <out.size(); i++)
        {
            parse_JSON_object(out[i], keys::SA, out2);
//            if (out2.size() == 0) { std::cout << "Phase name has to be specified in Data Target->OFUN->EPH!"<< std::endl; exit(1);} // ERROR
            SA.push_back(out2[0]);
            out2.clear();

            parse_JSON_object(out[i], keys::DS, out2);
//            if (out2.size() == 0) { std::cout << "Phase name has to be specified in Data Target->OFUN->EPH!"<< std::endl; exit(1);} // ERROR
            DS.push_back(out2[0]);
            out2.clear();

            parse_JSON_object(out[i], keys::WT, out2);
//            if (out2.size() == 0) { std::cout << "Phase name has to be specified in Data Target->OFUN->EPH!"<< std::endl; exit(1);} // ERROR
            WT.push_back(atof(out2[0].c_str()));
            out2.clear();
        }
        out.clear();
        parse_JSON_object(DataSelect, keys::datasetlist, out);
        Ndatasets = out.size();
        if ( (out.size() > 0) && (Nsamples > 0) )
        {
            std::cout << "You can't have samplelist and datasetlist options at the same time. Use only one at a time. " << std::endl;
            exit(1);
        }
        for (unsigned int i = 0; i <out.size(); i++)
        {

            parse_JSON_object(out[i], keys::DS, out2);
//            if (out2.size() == 0) { std::cout << "Phase name has to be specified in Data Target->OFUN->EPH!"<< std::endl; exit(1);} // ERROR
            DS.push_back(out2[0]);
            out2.clear();

            parse_JSON_object(out[i], keys::WT, out2);
//            if (out2.size() == 0) { std::cout << "Phase name has to be specified in Data Target->OFUN->EPH!"<< std::endl; exit(1);} // ERROR
            WT.push_back(atof(out2[0].c_str()));
            out2.clear();
        }

    }

    // Build the query in EJDB format

        // create EJDB database object
        static EJDB *jb;
        jb = ejdbnew();
        // open the database file as a writer JBOWRITER, create new is not existent JBOCREAT, and truncate db on open JBOTRUNC
std::cout << DBname.c_str() << std::endl;
        if (!ejdbopen(jb, DBname.c_str(), JBOREADER)) {
            std::cout << "Error opening the database" << std::endl; exit(1);
        }
// std::cout << gpf->GEMS3LstFilePath().c_str() << std::endl;
        //Get or create collection 'experiments'
        EJCOLL *coll = ejdbcreatecoll(jb, collection.c_str(), NULL);

        // Creat bson query object
        bson bq2;
        bson_init_as_query(&bq2);

        if ((Nsamples == 0))
        {
            // for selecting expdatasets
            if  ((usedataset.size() > 0) || (Ndatasets > 0))
            {

                    bson_append_start_object(&bq2, keys::expdataset);
                    bson_append_start_array(&bq2, "$in");
                    if ((usedataset.size() > 0))
                    if (!usedataset[0].empty())
                    {
                        for (unsigned int j=0; j<usedataset.size(); ++j)
                        {
                            ss << j;
                            sss = ss.str();
                            ss.str("");
                            bson_append_string(&bq2, sss.c_str(), usedataset[j].c_str());
                        }
                    }
                    // add datasetlist
                    if (Ndatasets > 0)
                    {
                        this->h_datasetlist = true;
                        for (unsigned int j=usedataset.size(); j<DS.size()+usedataset.size(); ++j)
                        {
                            ss << j;
                            sss = ss.str();
                            ss.str("");
                            bson_append_string(&bq2, sss.c_str(), DS[j-usedataset.size()].c_str());
                        }
                    }

                    bson_append_finish_array(&bq2);
                    bson_append_finish_object(&bq2);

            }

            // for skipping expdatasets
            if (skipdataset.size() > 0)
            {
                if (!skipdataset[0].empty())
                {
                    bson_append_start_object(&bq2, keys::expdataset);
                    bson_append_start_array(&bq2, "$nin");
                    for (unsigned int j=0; j<skipdataset.size(); ++j)
                    {
                        ss << j;
                        sss = ss.str();
                        ss.str("");
                        bson_append_string(&bq2, sss.c_str(), skipdataset[j].c_str());
                    }
                    bson_append_finish_array(&bq2);
                    bson_append_finish_object(&bq2);
                }
            }

            // for selecting usesamples
            if (usesample.size() > 0)
            {
                if (!usesample[0].empty())
                {
                    bson_append_start_object(&bq2, keys::expsample);
                    bson_append_start_array(&bq2, "$in");
                    for (unsigned int j=0; j<usesample.size(); ++j)
                    {
                        ss << j;
                        sss = ss.str();
                        ss.str("");
                        bson_append_string(&bq2, sss.c_str(), usesample[j].c_str());
         // std::cout << usesample[j].c_str() <<  std::endl;
                    }
                    bson_append_finish_array(&bq2);
                    bson_append_finish_object(&bq2);
                }
            }

            // for skipping skipsamples
            if (skipsample.size() > 0)
            {
                if (!skipsample[0].empty())
                {
                    bson_append_start_object(&bq2, keys::expsample);
                    bson_append_start_array(&bq2, "$nin");
                    for (unsigned int j=0; j<skipsample.size(); ++j)
                    {
                        ss << j;
                        sss = ss.str();
                        ss.str("");
                        bson_append_string(&bq2, sss.c_str(), skipsample[j].c_str());
         // std::cout << skipsample[j].c_str() <<  std::endl;
                    }
                    bson_append_finish_array(&bq2);
                    bson_append_finish_object(&bq2);
                }
            }

            // for selection of temperatures
            if (qsT.size() > 0)
            {
                if ((qsT.size() == 2))
                {
                    // for selecting T interval
                    bson_append_start_object(&bq2, keys::sT);
                    bson_append_start_array(&bq2, "$bt");
                    bson_append_double(&bq2, "0", qsT[0]);
                    bson_append_double(&bq2, "1", qsT[1]);
                    bson_append_finish_array(&bq2);
                    bson_append_finish_object(&bq2);
                } else
                    if (!(qsT[0] == 0))
                    {
                        bson_append_start_object(&bq2, keys::sT);
                        bson_append_start_array(&bq2, "$in");
                        for (unsigned int j=0; j<qsT.size(); ++j)
                        {
                            ss << j;
                            sss = ss.str();
                            ss.str("");
                            bson_append_int(&bq2, sss.c_str(), qsT[j]);
                        }
                        bson_append_finish_array(&bq2);
                        bson_append_finish_object(&bq2);
                    }
            }

            // for selection of pressures
            if (qsP.size() > 0)
            {
                if ((qsP.size() == 2))
                {
                    // for selecting P interval
                    bson_append_start_object(&bq2, keys::sP);
                    bson_append_start_array(&bq2, "$bt");
                    bson_append_double(&bq2, "0", qsP[0]);
                    bson_append_double(&bq2, "1", qsP[1]);
                    bson_append_finish_array(&bq2);
                    bson_append_finish_object(&bq2);
                } else
                    if (!(qsP[0] == 0))
                    {
                        bson_append_start_object(&bq2, keys::sP);
                        bson_append_start_array(&bq2, "$in");
                        for (unsigned int j=0; j<qsP.size(); ++j)
                        {
                            ss << j;
                            sss = ss.str();
                            ss.str("");
                            bson_append_int(&bq2, sss.c_str(), qsP[j]);
                        }
                        bson_append_finish_array(&bq2);
                        bson_append_finish_object(&bq2);
                    }
            }

            // for selecting pairs of datasets-samples
            if (usepdatasets.size() > 0)
            {
                if (!usepdatasets[0].empty())
                {
                    bson_append_start_array(&bq2, "$or");
                    unsigned int k=0; // counts trough the usesamples std::vector
                    int count = 0;
                    for (unsigned int j=0; j<usepdatasets.size(); ++j)
                    {
    //                    for (k; k<usepsamples.size(); k++)
                            while (k<usepsamples.size())
                        {
                            if (!(usepsamples[k] == ""))
                            {
                                ss << count;
                                sss = ss.str();
                                ss.str("");
                                bson_append_start_object(&bq2, sss.c_str());

                                bson_append_start_array(&bq2, "$and");

                                bson_append_start_object(&bq2, "0");
                                bson_append_string(&bq2, keys::expdataset, usepdatasets[j].c_str());
                                bson_append_finish_object(&bq2);

                                bson_append_start_object(&bq2, "1");
                                bson_append_string(&bq2, keys::expsample, usepsamples[k].c_str());
                                bson_append_finish_object(&bq2);

                                bson_append_finish_array(&bq2);
                                bson_append_finish_object(&bq2);

                                count++;

                            } else
                            {
                                k++;
                                break;
                            }
                            k++;
                        }
                    }
                    bson_append_finish_array(&bq2);
                }
            }
        } else // if we have samples list the search query is bulil as follows
        {
            // for selecting pairs of datasets-samples
            if (Nsamples > 0)
            {
                this->h_datasetlist = true;
            if (DS.size() > 0)
            {
                if (!DS[0].empty())
                {
                    bson_append_start_array(&bq2, "$or");
                    for  (unsigned k = 0; k<SA.size(); k++)
                    {
                        ss << k;
                        sss = ss.str();
                        ss.str("");
                        bson_append_start_object(&bq2, sss.c_str());
                        bson_append_start_array(&bq2, "$and");

                        bson_append_start_object(&bq2, "0");
                        bson_append_string(&bq2, keys::expdataset, DS[k].c_str());
                        bson_append_finish_object(&bq2);

                        bson_append_start_object(&bq2, "1");
                        bson_append_string(&bq2, keys::expsample, SA[k].c_str());
                        bson_append_finish_object(&bq2);

                        bson_append_finish_array(&bq2);
                        bson_append_finish_object(&bq2);
                    }
                    bson_append_finish_array(&bq2);
                }
            }
        }
        }

        // finish creading bson query object
        bson_finish(&bq2);
#ifdef CHECK_LOAD
    std::fstream ff3("DBquery.json", std::ios::out );
    ff3 << "{\n";
    bson_print_raw_txt( ff3, bq2.data, 0, BSON_OBJECT);
    ff3 << "}";
    ff3.close();
#endif


        EJQ *q2 = ejdbcreatequery(jb, &bq2, NULL, 0, NULL);

        uint32_t count;
        TCLIST *res = ejdbqryexecute(coll, q2, &count, 0, NULL);
        fprintf(stderr, "Records found: %d\n", count);

         // adding data into Data_manager storage class
         for (unsigned int j=0; j<count; j++)
         {
             experiments.push_back( new samples );
             // set experiments variables empty
             experiments[j]->sP = 0.; experiments[j]->sT = 0.; experiments[j]->sV= 0.;
             experiments[j]->idsample= 0;
             experiments[j]->weight = 1;
             // set experiments variables false
         }

         gpf->flog << "05. data_manager.cpp(365). Adding the data returned by the selection query into the data structure; " << std::endl;

#ifdef useomp
    omp_set_num_threads(this->MPI);
#ifdef buildWIN32
    #pragma omp parallel for schedule(static)
#else
    #pragma omp parallel for schedule(dynamic)
#endif
#endif
         for (int i = 0; i < TCLISTNUM(res); ++i) {
             void *bsdata = TCLISTVALPTR(res, i);
             char *bsdata_ = static_cast<char*>(bsdata);
             bson_to_Data_Manager(/*stderr,*/ bsdata_, i); // adding the data returned by the selection query into the data storage class
         }

        //Dispose result set
        tclistdel(res);

        //Dispose query
        ejdbquerydel(q2);
        bson_destroy(&bq2);

        //Close database
        ejdbclose(jb);
        ejdbdel(jb);

// Set weights provided in the sample list
        if (Nsamples > 0)
        {
            for (unsigned i = 0; i<experiments.size(); i++)
            {
                for (unsigned j = 0; j<Nsamples; j++)
                {
                    if ((experiments[i]->sample == SA[j]) && (experiments[i]->expdataset == DS[j]))
                    {
                        experiments[i]->weight = experiments[i]->weight * WT[j];
                    }
                }
            }
        }
// Set weights provided in the dataset list
        if (Ndatasets > 0)
        {
            for (unsigned i = 0; i<experiments.size(); i++)
            {
                for (unsigned j = 0; j<Ndatasets; j++)
                {
                    if (experiments[i]->expdataset == DS[j])
                    {
                        experiments[i]->weight = experiments[i]->weight * WT[j];
                    }
                }
            }
        }

        // for skipping expdataset-expsamples pairs
        if (skippdatasets.size() > 0)
        {
            if (!skippdatasets[0].empty())
            {
                unsigned int k=0; // counts torugh the skippsamples std::vector
                for (unsigned int j=0; j<skippdatasets.size(); ++j)
                {
//                    for (k; k<skippsamples.size(); k++)
                    while (k<skippsamples.size())
                    {
                        if (!(skippsamples[k] == ""))
                        {
                            for (unsigned int i = 0; i<this->experiments.size(); i++)
                            {
                                if ((this->experiments[i]->expdataset == skippdatasets[j]) && (this->experiments[i]->sample == skippsamples[k]))
                                {
                                    this->experiments.erase(this->experiments.begin() + i);
                                    count--;
                                }
                            }

                        } else
                        {
                            k++;
                            break;
                        }
                        k++;
                    }
                }
            }
            fprintf(stderr, "Records after removing skipped pairs (dataset-samples): %d\n", count);
        }

#ifdef CHECK_LOAD
    std::fstream test_out("experiments.log", std::ios::out);
    for (auto const& item : experiments) {
           test_out << *item << "\n";
    }
#endif
}

void Data_Manager::bson_to_Data_Manager(/* FILE *f, */ const char *data, int pos)
{
    bson_iterator i[1], j[1], k[1], k2[1], d[1], d2[1], d3[1]; // 1st, 2nd, 3rd, 2-1, 3-1 level
    int ip = -1, ic = -1, sk = -1, ipc, ipp, ips, ipdcp, ipm, ipam;

    bson_iterator_from_buffer(i, data); // getting primary iterator
    while ( bson_iterator_next(i) != BSON_EOO )
    {
        std::string key_ = bson_iterator_key(i);
//        key_ = key;

        if (key_ == keys::expdataset)
        {
            // adding expdataset
            experiments[pos]->expdataset = bson_iterator_string(i);
        } else
        if (key_ == keys::expsample)
        {
            // adding sample name
            experiments[pos]->sample = bson_iterator_string(i);
        } else
        if (key_ == keys::type)
        {
            // adding sample type
            experiments[pos]->Type = bson_iterator_string(i);
        } else
        if (key_ == keys::sT)
        {
            // adding temperature
            experiments[pos]->sT = bson_iterator_double(i);
        } else
        if (key_ == keys::sP)
        {
            // adding pressure
            experiments[pos]->sP = bson_iterator_double(i);
        } else
        if (key_ == keys::sV)
        {
            // adding volume
            experiments[pos]->sV = bson_iterator_double(i);
        } else
        if (key_ == keys::Weight)
        {
            // adding weight
//            if (!h_datasetlist)
            experiments[pos]->weight = bson_iterator_double(i);
        } else

        // adding experiment components/recipe
        if ((key_ == keys::sbcomp) ) // && (t == BSON_ARRAY))
        {
            bson_iterator_subiterator( i, j );
            while (bson_iterator_next(j) != BSON_EOO )
            {
                experiments[pos]->sbcomp.push_back( new samples::components );
                ic++; // position of the component in sbcomp std::vector
                experiments[pos]->sbcomp[ic]->Qnt = 0.;
                experiments[pos]->sbcomp[ic]->Qerror = 1.;
                experiments[pos]->sbcomp[ic]->Qunit = keys::gram; // default

                bson_iterator_subiterator( j, d );

                while (bson_iterator_next(d) != BSON_EOO )
                {
                    std::string key_ = bson_iterator_key(d);

                    if ((key_ == keys::comp))
                    {
                        experiments[pos]->sbcomp[ic]->comp =  bson_iterator_string(d) ;
                    } else
                    if ((key_ == keys::Qnt))
                    {
                        experiments[pos]->sbcomp[ic]->Qnt = bson_iterator_double(d) ;
                    } else
                    if ((key_ == keys::Qerror))
                    {
                        experiments[pos]->sbcomp[ic]->Qerror = bson_iterator_double(d) ;
                    } else
                    if ((key_ == keys::Qunit))
                    {
                        experiments[pos]->sbcomp[ic]->Qunit = bson_iterator_string(d) ;
                    }
                }
            }
        } else
            // adding experiment properties
            if ((key_ == keys::props) ) // && (t == BSON_ARRAY))
            {
                ic = -1;
                bson_iterator_subiterator( i, j );
                while (bson_iterator_next(j) != BSON_EOO )
                {
                    experiments[pos]->props.push_back( new samples::properties );
                    ic++; // position of the component in sbcomp std::vector
                    experiments[pos]->props[ic]->Qnt = 0.;
                    experiments[pos]->props[ic]->Qerror = 0.;
                    experiments[pos]->props[ic]->Qunit = "NULL"; // default

                    bson_iterator_subiterator( j, d );

                    while (bson_iterator_next(d) != BSON_EOO )
                    {
                        std::string key_ = bson_iterator_key(d);

                        if ((key_ == keys::property))
                        {
                            experiments[pos]->props[ic]->prop =  bson_iterator_string(d) ;
                        } else
                        if ((key_ == keys::Qnt))
                        {
                            experiments[pos]->props[ic]->Qnt = bson_iterator_double(d) ;
                        } else
                        if ((key_ == keys::Qerror))
                        {
                            experiments[pos]->props[ic]->Qerror = bson_iterator_double(d) ;
                        } else
                        if ((key_ == keys::Qunit))
                        {
                            experiments[pos]->props[ic]->Qunit = bson_iterator_string(d) ;
                        }
                    }
                }
            } else

        // adding Upper metastability constraints
//        if ((key_ == keys::UMC) ) // && (t == BSON_ARRAY))
//        {
//            bson_iterator_subiterator( i, j );
//            while (bson_iterator_next(j) != BSON_EOO )
//            {

//                experiments[pos]->U_KC.push_back( new samples::Uconstraints );
//                sk++; // position of the component in U_SK std::vector
////                experiments.at(pos)->U_KC.at(sk)->dcomp = NULL;
//                experiments[pos]->U_KC[sk]->Qnt = 1000000.;

//                bson_iterator_subiterator( j, d );
//                while (bson_iterator_next(d) != BSON_EOO )
//                {
//                    std::string key_ = bson_iterator_key(d);

//                    if ((key_ == keys::DC))
//                    {
//                        experiments[pos]->U_KC[sk]->type = keys::DC;
//                        experiments[pos]->U_KC[sk]->name =  bson_iterator_string(d) ;

//                    } else
//                    if ((key_ == keys::phase))
//                    {
//                        experiments[pos]->U_KC[sk]->type = keys::phase;
//                        experiments[pos]->U_KC[sk]->name = bson_iterator_string(d) ;
//                    } else
//                    if ((key_ == keys::Qnt))
//                    {
//                        experiments[pos]->U_KC[sk]->Qnt = bson_iterator_double(d) ;
//                    }
//                    /*else
//                    if ((key_ == keys::Qerror))
//                    {
//                        experiments.at(pos)->sbcomp.at(ic)->Qerror = bson_iterator_double(d) ;
//                    } else
//                    if ((key_ == keys::Qunit))
//                    {
//                        experiments.at(pos)->sbcomp.at(ic)->Qunit = bson_iterator_string(d) ;
//                    }*/
//                }
//            }
//        } else

//        // adding Lower metastability constraints
//        if ((key_ == keys::LMC) ) // && (t == BSON_ARRAY))
//        {
//            bson_iterator_subiterator( i, j );
//            while (bson_iterator_next(j) != BSON_EOO )
//            {

//                experiments[pos]->L_KC.push_back( new samples::Lconstraints );
//                sk++; // position of the component in U_SK std::vector
//    //            experiments.at(pos)->U_KC.at(sk)->dcomp = NULL;
//                experiments[pos]->L_KC[sk]->Qnt = 0.;

//                bson_iterator_subiterator( j, d );
//                while (bson_iterator_next(d) != BSON_EOO )
//                {
//                    std::string key_ = bson_iterator_key(d);

//                    if ((key_ == keys::DC))
//                    {
//                        experiments[pos]->L_KC[sk]->type = keys::DC;
//                        experiments[pos]->L_KC[sk]->name =  bson_iterator_string(d) ;

//                    } else
//                    if ((key_ == keys::phase))
//                    {
//                        experiments[pos]->L_KC[sk]->type = keys::phase;
//                        experiments[pos]->L_KC[sk]->name = bson_iterator_string(d) ;
//                    } else
//                    if ((key_ == keys::Qnt))
//                    {
//                        experiments[pos]->U_KC[sk]->Qnt = bson_iterator_double(d) ;
//                    }
//                     /*else
//                        if ((key_ == keys::Qerror))
//                        {
//                            experiments.at(pos)->sbcomp.at(ic)->Qerror = bson_iterator_double(d) ;
//                        } else
//                        if ((key_ == keys::Qunit))
//                        {
//                            experiments.at(pos)->sbcomp.at(ic)->Qunit = bson_iterator_string(d) ;
//                        }*/
//                }
//           }
//        } else

        // adding experimental phases
        if ((key_ == keys::expphases) ) // && (t == BSON_ARRAY))
        {
            bson_iterator_subiterator( i, j );
            while ( bson_iterator_next(j) != BSON_EOO )
            {

                experiments[pos]->expphases.push_back( new samples::phases );
                ip++; // position of the phase in expphases std::vector
                ipc = -1; // phases components - reset to -1 for every new phase
                ipm = -1; // molar fractions
                ipp = -1; // phases properties
                ipam= -1; // phase act model
                ips = -1; // phases dcomps
                experiments[pos]->expphases[ip]->idphase = 0;

                bson_iterator_subiterator( j, d );
                while (bson_iterator_next(d) != BSON_EOO )
                {
                    std::string key_ = bson_iterator_key(d);

                    if ((key_ == keys::phase))
                    {
                        experiments[pos]->expphases[ip]->phase =  bson_iterator_string(d) ;
                    } else

                    // adding phase components
                    if ((key_ == keys::phIC) ) //  && (t == BSON_ARRAY))
                    {
                        bson_iterator_subiterator( d, k );
                        while ( bson_iterator_next(k) != BSON_EOO )
                        {
                            experiments[pos]->expphases[ip]->phIC.push_back( new samples::components );
                            ipc++; // position of the component in phcomp std::vector
                            // default values
                            experiments[pos]->expphases[ip]->phIC[ipc]->Qerror = 1.;
                            experiments[pos]->expphases[ip]->phIC[ipc]->Qnt   = 0.;
                            std::string p_name = experiments[pos]->expphases[ip]->phase;
//                            if (p_name == keys::aqueous)
//                            {
//                                experiments[pos]->expphases[ip]->phIC[ipc]->Qunit = keys::molal;
//                            } else
//                                experiments[pos]->expphases[ip]->phIC[ipc]->Qunit = keys::molratio;

                            bson_iterator_subiterator( k, d2 );
                            while ( bson_iterator_next(d2) != BSON_EOO )
                            {
                                std::string key_ = bson_iterator_key(d2);

                                if ((key_ == keys::IC))
                                {
                                    experiments[pos]->expphases[ip]->phIC[ipc]->comp = bson_iterator_string(d2) ;
                                } else
                                if ((key_ == keys::Qnt))
                                {
                                    experiments[pos]->expphases[ip]->phIC[ipc]->Qnt = bson_iterator_double(d2) ;
                                } else
                                if ((key_ == keys::Qerror))
                                {
                                    experiments[pos]->expphases[ip]->phIC[ipc]->Qerror = bson_iterator_double(d2) ;
                                } else
                                if ((key_ == keys::Qunit))
                                {
                                    experiments[pos]->expphases[ip]->phIC[ipc]->Qunit = bson_iterator_string(d2) ;
                                }
                            }
                        }
                    } else

                    // adding phase IC as mole fractions MR
                    if ((key_ == keys::phMR) ) // && (t == BSON_ARRAY))
                    {
                        bson_iterator_subiterator( d, k );
                        while (bson_iterator_next(k) != BSON_EOO )
                        {
                            experiments[pos]->expphases[ip]->phMR.push_back( new samples::components );
                            ipm++; // position of the component in phcomp std::vector
                            experiments[pos]->expphases[ip]->phMR[ipm]->Qerror = 1.;
                            experiments[pos]->expphases[ip]->phMR[ipm]->Qnt   = 0.;
                            std::string p_name = experiments[pos]->expphases[ip]->phase;
//                            if (p_name == keys::aqueous)
//                            {
//                                experiments[pos]->expphases[ip]->phMR[ipm]->Qunit = keys::molratio;
//                            } else
                                experiments[pos]->expphases[ip]->phMR[ipm]->Qunit = keys::molratio;

                            bson_iterator_subiterator( k, d2 );
                            while (bson_iterator_next(d2) != BSON_EOO )
                            {
                                std::string key_ = bson_iterator_key(d2);

                                if ((key_ == keys::MR))
                                {
                                    experiments[pos]->expphases[ip]->phMR[ipm]->comp = bson_iterator_string(d2) ;
                                } else
                                if ((key_ == keys::Qnt))
                                {
                                    experiments[pos]->expphases[ip]->phMR[ipm]->Qnt = bson_iterator_double(d2) ;
                                 } else
                                 if ((key_ == keys::Qerror))
                                 {
                                    experiments[pos]->expphases[ip]->phMR[ipm]->Qerror = bson_iterator_double(d2) ;
                                 } else
                                 if ((key_ == keys::Qunit))
                                 {
                                    experiments[pos]->expphases[ip]->phMR[ipm]->Qunit = bson_iterator_string(d2) ;
                                 }
                             }
                        }
                    } else

                    // adding phase properties
                    if ((key_ == keys::phprop) ) // && (t == BSON_ARRAY))
                    {
                        bson_iterator_subiterator( d, k );
                        while ( bson_iterator_next(k) != BSON_EOO )
                        {
                            experiments[pos]->expphases[ip]->phprop.push_back( new samples::phases::prop );
                            ipp++; // position of the component in phcomp std::vector
                            experiments[pos]->expphases[ip]->phprop[ipp]->Qerror = 1.;
                            experiments[pos]->expphases[ip]->phprop[ipp]->Qnt   = 0.;
//std::cout << "pos: " << pos << " ip: " << ip << " ipp: " << ipp << " :: ";
                            bson_iterator_subiterator( k, d2 );
                            while (bson_iterator_next(d2) != BSON_EOO )
                            {
                                std::string key_ = bson_iterator_key(d2);
//std::cout << " " << key_.c_str();
                                if ((key_ == keys::property))
                                {
                                    std::string p_name = bson_iterator_string(d2);
//std::cout << ": " << p_name;
                                    experiments[pos]->expphases[ip]->phprop[ipp]->property = p_name; // bson_iterator_string(d2) ;
                                    // assigining default values for units
                                    if (p_name==keys::Qnt)
                                    {
                                        experiments[pos]->expphases[ip]->phprop[ipp]->Qunit = keys::gram;
                                    } else
                                    if (p_name==keys::RHO)
                                    {
                                        experiments[pos]->expphases[ip]->phprop[ipp]->Qunit = keys::g_cm3;
                                    } else
                                    if (p_name==keys::Gex)
                                    {
                                        experiments[pos]->expphases[ip]->phprop[ipp]->Qunit = keys::J_mol;
                                    } else
                                    if (p_name==keys::pV)
                                    {
                                        experiments[pos]->expphases[ip]->phprop[ipp]->Qunit = keys::cm3;
                                    } else
                                    if (p_name==keys::pH)
                                    {
                                        experiments[pos]->expphases[ip]->phprop[ipp]->Qunit = keys::_loga;
                                    }
                                    if (p_name==keys::Eh)
                                    {
                                        experiments[pos]->expphases[ip]->phprop[ipp]->Qunit = keys::Volts;
                                    } else
                                    if (p_name==keys::pe)
                                    {
                                        experiments[pos]->expphases[ip]->phprop[ipp]->Qunit = keys::_loga;
                                    } else
                                    if (p_name==keys::oscw)
                                    {
//                                        experiments[pos]->expphases[ip]->phprop[ipp]->Qunit = keys::_loga;
                                    }
                                } else
                                if ((key_ == keys::Qnt))
                                {
                                    double qw = bson_iterator_double(d2) ;
                                    experiments[pos]->expphases[ip]->phprop[ipp]->Qnt = qw;
//std::cout << ": " << qw;
                                } else
                                if ((key_ == keys::Qerror))
                                {
                                    double qw = bson_iterator_double(d2) ;
                                    experiments[pos]->expphases[ip]->phprop[ipp]->Qerror = qw ;
//std::cout << ": " << qw;
                                } else
                                if ((key_ == keys::Qunit))
                                {
                                    std::string unit = bson_iterator_string(d2) ;
                                    experiments[pos]->expphases[ip]->phprop[ipp]->Qunit = unit ;
//std::cout << ": " << unit;
                                }
                            }
                        }
//std::cout << std::endl;
                    } else

                    // adding phase activity_model
                    if ((key_ == keys::activity_model) ) // && (t == BSON_ARRAY))
                        {
                                experiments[pos]->expphases[ip]->phactmod.isActMod = true;
                                bson_iterator_subiterator( k, d2 );
                                while (bson_iterator_next(d2) != BSON_EOO )
                                {
                                    std::string key_ = bson_iterator_key(d2);
                                    if ((key_ == keys::b_gamma))
                                    {
                                        double qw = bson_iterator_double(d2) ;
                                        experiments[pos]->expphases[ip]->phactmod.b_gamma = qw;
                                    } else
                                    if ((key_ == keys::b_gammaT))
                                    {
                                        std::string qw = bson_iterator_string(d2) ;
                                        experiments[pos]->expphases[ip]->phactmod.b_gammaT = qw ;
                                    } else
                                    if ((key_ == keys::a0))
                                    {
                                        double qw = bson_iterator_double(d2) ;
                                        experiments[pos]->expphases[ip]->phactmod.a0 = qw ;
                                    } else
                                    if ((key_ == keys::gammaN))
                                    {
                                        int qw = bson_iterator_int(d2) ;
                                        experiments[pos]->expphases[ip]->phactmod.gammaN = qw ;
                                    } else
                                    if ((key_ == keys::gammaW))
                                    {
                                        int qw = bson_iterator_int(d2) ;
                                        experiments[pos]->expphases[ip]->phactmod.gammaW = qw ;
                                    }
                                }
//                            }
    //cout << endl;
                    } else

                    // adding phase dcomps
                    if ((key_ == keys::phDC) ) // && (t == BSON_ARRAY))
                    {
                        bson_iterator_subiterator( d, k );
                        while (bson_iterator_next(k) != BSON_EOO )
                        {
                            experiments[pos]->expphases[ip]->phDC.push_back( new samples::phases::dcomps );
                            ips++; // position of the specie in phdcomps std::vector
                            ipdcp = -1;

                            bson_iterator_subiterator( k, d2 );
                            while (bson_iterator_next(d2) != BSON_EOO )
                            {
                                std::string key_ = bson_iterator_key(d2);

                                if ((key_ == keys::DC)) // adding the name of the specie
                                {
                                    experiments[pos]->expphases[ip]->phDC[ips]->DC =  bson_iterator_string(d2) ;
                                } else

                                // adding dependent compoponents properties
                                if ((key_ == keys::DCprop) ) // && (t == BSON_ARRAY))
                                {
                                    bson_iterator_subiterator( d2, k2 );
                                    while (bson_iterator_next(k2) != BSON_EOO )
                                    {
                                        experiments[pos]->expphases[ip]->phDC[ips]->DCprop.push_back( new samples::phases::dcomps::dcprop );
                                        ipdcp++; // position of the component in phcomp std::vector
                                        experiments[pos]->expphases[ip]->phDC[ips]->DCprop[ipdcp]->Qerror = 1.;
                                        experiments[pos]->expphases[ip]->phDC[ips]->DCprop[ipdcp]->Qnt   = 0.;

                                        bson_iterator_subiterator( k2, d3 );
                                        while (bson_iterator_next(d3) != BSON_EOO )
                                        {
                                            std::string key_ = bson_iterator_key(d3);

                                            if ((key_ == keys::property))
                                            {
                                                experiments[pos]->expphases[ip]->phDC[ips]->DCprop[ipdcp]->property = bson_iterator_string(d3);
                                            } else
                                            if ((key_ == keys::Qnt))
                                            {
                                                experiments[pos]->expphases[ip]->phDC[ips]->DCprop[ipdcp]->Qnt = bson_iterator_double(d3) ;
                                            } else
                                            if ((key_ == keys::Qerror))
                                            {
                                                experiments[pos]->expphases[ip]->phDC[ips]->DCprop[ipdcp]->Qerror = bson_iterator_double(d3) ;
                                            } else
                                            if ((key_ == keys::Qunit))
                                            {
                                                experiments[pos]->expphases[ip]->phDC[ips]->DCprop[ipdcp]->Qunit = bson_iterator_string(d3) ;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
//std::cout << std::endl;
}

void Data_Manager::get_distinct_TP( )
{
    std::vector<double> TP[2];
    unsigned int i, j;
    bool isfound = false, isfound2 = false;

    for (i=0; i<experiments.size(); ++i)
    {
        TP[0].push_back(experiments[i]->sT);
        TP[1].push_back(experiments[i]->sP);
    }
    for (i=0; i<TP[0].size(); i++)
    {
        // check if TP pair is presnt more than once in the TP std::vector
        for (j=0; j<TP[0].size(); j++)
        {
            if ((TP[0][i] == TP[0][j]) && (TP[1][i] == TP[1][j]) && (i != j))
            {
                isfound = true;
            }
        }
        // check if TP pair was added to the unique TP pairs container
        for (j=0; j<TP_pairs[0].size(); ++j)
        {
            if ((TP[0][i] == TP_pairs[0][j]) && (TP[1][i] == TP_pairs[1][j]))
            {
                isfound2 = true;
            }
        }
        // add TP pair if it does not repeat itself or was not added already in the container
        if ((!isfound) || (!isfound2))
        {
            TP_pairs[0].push_back(TP[0][i]);
            TP_pairs[1].push_back(TP[1][i]);
        }
        isfound = false;
        isfound2 = false;
    }   
}

std::ostream& operator<<(std::ostream& stream, const Data_Manager::DataSynonyms& data) {
    stream << data.GemsName << " : ";
    for (auto const& item : data.syn) {
        stream << "" << item << "\n";
    }
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const Data_Manager::PhSyn& data) {
    stream << data.GemsName << " : ";
    for (auto const& item : data.syn) {
        stream << " " << item;
    }
    stream << "\nSynDCs\n";
    for (auto const& item : data.SynDCs) {
        stream << "   " << item << "\n";
    }
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const Data_Manager::samples& data_lst) {
    stream << "----------------------------\n";
    stream << data_lst.idsample << " " << data_lst.sample << " " << data_lst.expdataset << " " << data_lst.Type << " " << "\n";
    stream << data_lst.weight << " " << data_lst.sT << " " << data_lst.Tunit << " " << data_lst.sP << " "
           << data_lst.Punit << " " << data_lst.sV << " " << data_lst.Vunit << "\n";

    stream << " sbcomp: \n";
    for (auto const& item : data_lst.sbcomp) {
        stream << "    " << item->comp << " " << item->Qnt << " " << item->Qerror << " " << item->Qunit  << "\n";
    }
    stream << " props: \n";
    for (auto const& item : data_lst.props) {
        stream << "    " << item->prop << " " << item->Qnt << " " << item->Qerror << " " << item->Qunit  << "\n";
    }
    stream << " expphases: \n";
    for (auto const& item_ph : data_lst.expphases) {
        stream << item_ph->idphase << " " << item_ph->phase << " " << "\n";
        stream << " phprop: \n";
        for (auto const& item : item_ph->phprop) {
            stream << "    " << item->property << " " << item->Qnt << " " << item->Qerror << " " << item->Qunit  << "\n";
        }
        stream << " phIC: \n";
        for (auto const& item : item_ph->phIC) {
            stream << "    " << item->comp << " " << item->Qnt << " " << item->Qerror << " " << item->Qunit  << "\n";
        }
        stream << " phMR: \n";
        for (auto const& item : item_ph->phMR) {
            stream << "    " << item->comp << " " << item->Qnt << " " << item->Qerror << " " << item->Qunit  << "\n";
        }
        stream << " phDC: \n";
        for (auto const& item_dc : item_ph->phDC) {
            stream << item_dc->DC << "\n";
            for (auto const& item : item_dc->DCprop) {
                stream << "    " << item->property << " " << item->Qnt << " " << item->Qerror << " " << item->Qunit  << "\n";
            }
        }
    }
    stream << std::endl;
    return stream;
}


